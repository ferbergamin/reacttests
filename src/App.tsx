import React, { Component } from 'react';
// import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonContent,
  IonPage,
  IonToolbar,
  IonButtons,
  IonButton,
  IonIcon,
  IonSearchbar
} from '@ionic/react';
// import { IonApp, IonRouterOutlet, IonTabs, IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react';
// import { home, helpCircleOutline } from 'ionicons/icons';
import { IonReactRouter } from '@ionic/react-router';
// import { Home, Fim, Login } from './pages';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import { userContext } from './helpers/userContext';
import { RouterComponent, Header } from './components';
import {
  logOut,
} from 'ionicons/icons';

interface state {
  idUser: any,
  nome: any,
  email: any,
  username: any,
  criadoEm: any,
  role: any
  // getUser: (token: String) => {}
}

class App extends Component<any, state>{
  getUser: (token: string) => void;
  constructor(props: any) {
    super(props);
    this.getUser = (token: string) => {
      let user = localStorage.getItem("token");
      if (user) {
        this.setState(
          JSON.parse(user)[token]
        )
      }
    }

    this.state = {
      idUser: undefined,
      nome: undefined,
      email: undefined,
      username: undefined,
      criadoEm: undefined,
      role: undefined
    }
  }

  getToken() {
    const token = localStorage.getItem('token');
    if (token) {
      for (let key in JSON.parse(token)) {
        this.getUser(key);
        break;
      };
    }
  }

  componentDidMount() {
    this.getToken()
  }

  render() {
    function logOutFunction() { localStorage.removeItem("token"); window.location.href = "/" };
    return (
      <userContext.Provider value={{ ...this.state, getUser: this.getUser }} >
        <IonApp>

          {this.state.idUser &&
            <IonPage>
              {/* <IonHeader> */}
                <IonToolbar color="primary">
                  <IonSearchbar
                    placeholder="Pesquisar"
                  />
                  <IonButtons slot="end">
                    <IonButton onClick={logOutFunction}>
                      <IonIcon icon={logOut} />
                    </IonButton>
                  </IonButtons>
                  {/* <IonTitle>My Navigation Bar</IonTitle> */}
                </IonToolbar>
              {/* </IonHeader> */}
              <IonContent>
                <Header />
              </IonContent>
            </IonPage>
          }
          {!this.state.idUser &&
            <IonReactRouter>
              <RouterComponent />
            </IonReactRouter>
          }
        </IonApp>
      </userContext.Provider >
    );
  }
}
export default App;



// {/* <IonRouterOutlet>
//               <Route path="/login"
//                 component={this.state.idUser ? Home : Login} />
//               <Route exact path="/" render={() => {
//                 if (this.state.idUser) {
//                   return <Redirect to="/home" />
//                 }
//                 return <Redirect to="/login" />
//               }} />

//               <RouterController exact path="/home" component={Home} />
//             </IonRouterOutlet> */}
//               {/* <IonTabs>
//               <IonTabBar slot="bottom">
//                 <IonTabButton href="/" tab="">
//                   <IonIcon icon={home} />
//                   <IonLabel>{this.state.idUser ? "Início" : "Login"}</IonLabel>
//                 </IonTabButton>
//                 <IonTabButton href="/fim" tab="fim">
//                   <IonIcon icon={helpCircleOutline} />
//                   <IonLabel>Sobre</IonLabel>
//                 </IonTabButton>
//               </IonTabBar>
//             </IonTabs> */}