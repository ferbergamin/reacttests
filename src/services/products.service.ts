import axios, { AxiosResponse } from 'axios';

export const productServices = {
    listProducts
}

async function listProducts(): Promise<AxiosResponse> {
    return new Promise((resolve, reject) =>
        axios({
            method: "GET",
            url: "https://jsonplaceholder.typicode.com/photos"
        }).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        })
    )
}