import axios, { AxiosResponse } from 'axios';

const URL = "https://api.lelmarketingdigital.now.sh/user";

export const userServices = {
    Login
}

async function Login(data: any): Promise<AxiosResponse> {
    data["roleAdmin"] = "Admin";
    return new Promise((resolve, reject) => {
        return axios({
            method: "POST",
            url: `${URL}/login`,
            data: data
        }).then((response) => {
            resolve(response);
        }).catch(error => {
            reject(error);
        })
    })
}