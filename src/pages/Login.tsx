import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton, IonLabel, IonInput, IonItem, IonRow, IonToast, IonCol, IonIcon } from '@ionic/react';
import React, { Component, FormEvent } from 'react';
import './index.css';
import { userContext } from '../helpers/userContext';
import { userServices } from '../services/user.service';
import { informationCircle } from 'ionicons/icons';
import { Link } from 'react-router-dom';

class Login extends Component<any, any>{
  _mounted: boolean;

  constructor(props: any) {
    super(props);
    this._mounted = false;
    this.state = {
      email: "",
      password: "",
      submitted: false,
      toastOpen: false,
      messageToast: ""
    }

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this._mounted = true;
  }

  async handleSubmit(e: FormEvent) {
    e.preventDefault();
    this.setState({
      submitted: true
    });

    if (this._mounted) {

      await userServices.Login({
        emailOrUsername: this.state.email,
        senha: this.state.password
      }).then((response) => {
        if (response.status === 201) {
          let data = response.data.response;
          let token = data.token;
          localStorage.setItem("token", JSON.stringify({ [token]: data.data }));

          this.context.getUser(token);
          window.location.href="/home";
        }
      }).catch(err => {
        this.setState({
          submitted: false,
          messageToast: "Erro ao fazer login, verique sua senha e email e tente novamente",
          toastOpen: true,
          password: ""
        });
      });
    }
    // console.log(this.state)
  }

  render() {
    return (

      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Login</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent class="ion-padding">
          <IonRow style={{ height: "90%", alignItems: "center", justifyContent: "center" }}>
            <IonCol size="12">
              <form onSubmit={this.handleSubmit}>
                <IonItem>
                  <IonLabel position="stacked">Email</IonLabel>
                  <IonInput onInput={(e: any) => {
                    this.setState({ email: e.target.value });
                  }} value={this.state.email} required type="email" />
                </IonItem>
                <IonItem class="ion-margin-top">
                  <IonLabel position="stacked">Senha</IonLabel>
                  <IonInput onInput={(e: any) => {
                    this.setState({ password: e.target.value });
                  }} value={this.state.password} required type="password" />
                </IonItem>
                <IonRow class="ion-justify-content-center ion-margin-top">
                  <IonButton disabled={this.state.submitted} type="submit">Entrar</IonButton>
                </IonRow>
              </form>
              <IonRow style={{ alignItems: "center", justifyContent: "center"}}>
                <IonCol size="12">
                  <Link to="/">
                    <IonLabel>
                      Cadastro
                    </IonLabel>
                  </Link>
                </IonCol>
                <IonCol size="12">
                  <Link to="/">
                    <IonLabel>
                      Esqueci minha senha
                    </IonLabel>
                  </Link>
                </IonCol>
              </IonRow>
            </IonCol>

          </IonRow>
          <IonRow>
            <IonCol size="12">
              <IonRow class="ion-justify-content-end">
                <Link to="/end">
                  <IonIcon size="large" icon={informationCircle} />
                </Link>
              </IonRow>
            </IonCol>
          </IonRow>
          <IonToast
            isOpen={this.state.toastOpen}
            onDidDismiss={() => this.setState({
              toastOpen: false
            })}
            color="danger"
            message={this.state.messageToast}
            duration={3000}
            buttons={
              [{
                side: 'end',
                icon: 'close',
                // text: 'Favorite',
                handler: () => this.setState({
                  toastOpen: false
                })
              }]
            }
          />
        </IonContent>

      </IonPage>
    );
  }
};

Login.contextType = userContext;

export default Login;
