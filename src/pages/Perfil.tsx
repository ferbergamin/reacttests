import React, { Component } from 'react';
import {
    IonPage,
    IonContent,
    IonCard,
    IonCardHeader,
    IonText,
    IonIcon,
    IonImg,
    IonCardContent,
    IonLabel
} from '@ionic/react';
import {
    star,
    starHalf
} from 'ionicons/icons';
import { userContext } from '../helpers/userContext';

class Perfil extends Component {
    render() {
        return (
            <IonPage>
                <IonContent>
                    <IonCard>
                        <IonCardHeader>
                            <IonImg src="https://via.placeholder.com/600/92c952" />
                        </IonCardHeader>
                        <IonCardContent>
                            <IonText color="dark">{this.context.nome}</IonText>
                            <IonLabel class="ion-padding">
                                <IonIcon size="small" color="success" icon={star} />
                                <IonIcon size="small" color="success" icon={star} />
                                <IonIcon size="small" color="success" icon={star} />
                                <IonIcon size="small" color="success" icon={star} />
                                <IonIcon size="small" color="success" icon={starHalf} />
                            </IonLabel>
                        </IonCardContent>
                    </IonCard>
                </IonContent>
            </IonPage>
        )
    }
}

Perfil.contextType = userContext;
export default Perfil;