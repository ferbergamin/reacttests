import React from 'react';
import {
    IonPage,
    IonContent,
    IonGrid,
    IonRow,
    IonCol,
    IonImg,
    IonText
} from '@ionic/react';

export const Shop: React.FC = () => {
    return (
        <IonPage>
            <IonContent>
                <IonGrid class="ion-text-center">
                    <IonRow>
                        <IonCol size="6">
                            <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/92c952" />
                            <IonText>
                                <h4>Categoria 1</h4>
                            </IonText>
                        </IonCol>
                        <IonCol size="6">
                            <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/771796" />
                            <IonText>
                                <h4>Categoria 2</h4>
                            </IonText>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol size="6">
                            <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/d32776" />
                            <IonText>
                                <h5>Categoria 4</h5>
                            </IonText>
                        </IonCol>
                        <IonCol size="6">
                            <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/54176f" />
                            <IonText>
                                <h5>Categoria 4</h5>
                            </IonText>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol size="6">
                            <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/b0f7cc" />
                            <IonText>
                                <h4>Categoria 5</h4>
                            </IonText>
                        </IonCol>
                        <IonCol size="6">
                            <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/810b14" />
                            <IonText>
                                <h4>Categoria 6</h4>
                            </IonText>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    )
}