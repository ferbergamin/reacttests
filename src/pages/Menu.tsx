import React, { Component } from 'react';
import {
    IonPage,
    IonContent,
    IonList,
    IonItem,
    IonIcon,
    IonLabel
} from '@ionic/react';
import {
    home,
    informationCircleOutline,
    person,
} from 'ionicons/icons';
import { Link } from 'react-router-dom';

class Menu extends Component {
    render() {
        return (
            <IonPage>
                <IonContent>
                    <IonList>
                        <Link style={{ color: "transparent" }} to="/home">
                            <IonItem >
                                <IonIcon slot="start" icon={home} />
                                <IonLabel>Início</IonLabel>
                            </IonItem>
                        </Link>

                        <Link style={{ color: "transparent" }} to="/account">
                            <IonItem >
                                <IonIcon slot="start" icon={person} />
                                <IonLabel>Minha conta</IonLabel>
                            </IonItem>
                        </Link>

                        <Link style={{ color: "transparent" }} to="/end">
                            <IonItem >
                                <IonIcon slot="start" icon={informationCircleOutline} />
                                <IonLabel>Informações sobre o app</IonLabel>
                            </IonItem>
                        </Link>

                    </IonList>
                </IonContent>
            </IonPage>
        )
    }
}

export default Menu;