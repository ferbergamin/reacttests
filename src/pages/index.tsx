export { default as Home } from './Home';
export { default as Fim } from './Fim';
export { default as Login } from './Login';
export { default as Perfil } from './Perfil';
export { default as Menu } from './Menu';
export * from './Shop';