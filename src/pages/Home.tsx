import React, { Component } from 'react';
import {
    IonText,
    IonRow,
    IonPage,
    IonContent,
    IonLoading,
    IonCol,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonImg,
    IonGrid,
    IonFabButton,
    IonIcon,
    IonItem,
    IonLabel,
    IonRefresher,
    IonRefresherContent,
    IonSlides,
    IonSlide,
    IonItemDivider
} from '@ionic/react';
import { userContext } from '../helpers/userContext';
import { withRouter } from 'react-router';
import { productServices } from '../services/products.service';
import { cart, add } from 'ionicons/icons';
import { Link } from 'react-router-dom';
import { RefresherEventDetail } from '@ionic/core';

class Home extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            products: [],
            productsToShow: [],
            mounted: false,
            itemsToShow: 10
        }
        this.loadMore = this.loadMore.bind(this);
        this.refresh = this.refresh.bind(this);
        this.getProducts = this.getProducts.bind(this);
    }

    componentDidMount() {
        this.getProducts();
    }

    async getProducts() {
        await productServices.listProducts().then(response => {
            this.setState({
                products: response.data,
                productsToShow: response.data.slice(0, 10),
                mounted: true
            });
        }).catch(err => window.alert(err.toString()))
    }

    loadMore() {
        if (this.state.products.length >= this.state.productsToShow.length) {
            this.setState({
                itemsToShow: this.state.itemsToShow + 10,
                productsToShow: this.state.products.slice(0, this.state.itemsToShow + 10)
            })
        }
    }

    refresh(event: CustomEvent<RefresherEventDetail>) {
        this.getProducts().then(() => event.detail.complete());
    }
    render() {
        return (
            <IonPage>
                <IonContent>
                    <IonCol class="ion-padding-top" size="12">
                        <IonText class="ion-padding-start">Lojas premium</IonText>
                    </IonCol>
                    <IonGrid>
                        <IonSlides>
                            <IonSlide>
                                <IonRow>

                                    <IonCol size="4">
                                        <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/92c952" />
                                        <IonText>Loja de Ryer</IonText>
                                    </IonCol>
                                    <IonCol size="4">
                                        <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/92c952" />
                                        <IonText>Loja de calixto</IonText>
                                    </IonCol>
                                    <IonCol size="4">
                                        <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/92c952" />
                                        <IonText>Loja de {this.context.username}</IonText>
                                    </IonCol>
                                </IonRow>
                            </IonSlide>
                            <IonSlide>
                                <IonRow>
                                    <IonCol size="4">
                                        <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/92c952" />
                                        <IonText>Loja de jorge</IonText>
                                    </IonCol>
                                    <IonCol size="4">
                                        <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/92c952" />
                                        <IonText>Loja de maiky</IonText>
                                    </IonCol>
                                    <IonCol size="4">
                                        <IonImg style={{ height: "auto", width: "auto" }} src="https://via.placeholder.com/600/92c952" />
                                        <IonText>Loja de silva</IonText>
                                    </IonCol>
                                </IonRow>
                            </IonSlide>
                        </IonSlides>
                    </IonGrid>
                    <IonItemDivider />
                    <IonCol size="12">
                        <IonText class="ion-padding-start">Destaques</IonText>
                        <br />
                    </IonCol>

                    <IonRefresher onIonRefresh={this.refresh} slot="fixed">
                        <IonRefresherContent>

                        </IonRefresherContent>
                    </IonRefresher>
                    <IonGrid>
                        <IonRow class="ion-justify-content-center">
                            {this.state.mounted &&
                                Object.keys(this.state.productsToShow).map(key => {
                                    let product = this.state.productsToShow[key];
                                    return (
                                        <IonCol key={key} size="6">
                                            <IonCard>
                                                <IonCardHeader>
                                                    <IonImg src={product.url} />
                                                </IonCardHeader>
                                                <IonCardContent>
                                                    <IonLabel color="dark" >
                                                        {product.title}
                                                    </IonLabel>
                                                    <br />
                                                    <IonText>
                                                        R$ {product.albumId.toFixed(2)} ou ♢{product.albumId}0/{product.albumId}0
                                                    </IonText>
                                                    <br />
                                                    <IonText>
                                                        Por: <Link to="/perfil">{this.context.username}</Link>
                                                    </IonText>

                                                </IonCardContent>
                                                <IonItem>
                                                    <IonFabButton color="dark" size="small" slot="end">
                                                        <IonIcon color="light" icon={cart} />
                                                    </IonFabButton>
                                                </IonItem>
                                            </IonCard>
                                        </IonCol>
                                    )
                                })
                            }
                            {
                                this.state.mounted &&
                                <IonFabButton onClick={this.loadMore}>
                                    <IonIcon icon={add} />
                                </IonFabButton>
                            }
                            <IonLoading
                                isOpen={!this.state.mounted}
                                message="Aguarde..."
                                duration={50000}
                                showBackdrop={false}
                                spinner="circular"
                            />
                        </IonRow>
                    </IonGrid>
                </IonContent>
            </IonPage >

        )
    }
}

Home.contextType = userContext;
export default withRouter(Home);