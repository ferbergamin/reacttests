import { IonContent, IonPage, IonRow, IonText, IonIcon } from '@ionic/react';
import React from 'react';
import { logoIonic } from 'ionicons/icons'
import { withRouter } from 'react-router';

const Fim: React.SFC<any> = () => {
  return (
    <IonPage>
      <IonContent class="ion-padding">
        <IonRow>
          <IonText class="ion-text-center">
            <IonIcon icon={logoIonic} size="large" />
            <br />
            Aplicativo desenvolvido por Luiz F Bergamin com <b>Ionic React</b> para fins de aprendizado
          </IonText>
        </IonRow>
      </IonContent>
    </IonPage>
  );
};
export default withRouter(Fim);