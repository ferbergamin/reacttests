import React, { Component } from 'react';
import { userContext } from './userContext';
import { Route, Redirect } from 'react-router';

class RouterController extends Component<any, any>{
    constructor(props: any) {
        super(props);

        this.state = {

        }
    }
    render() {
        const { component: Comp, ...rest } = this.props;
        return (
            this.context.idUser ? <Route {...rest} component={Comp} /> : <Redirect to="/login" />
        )
    }
}

RouterController.contextType = userContext;
export default RouterController;