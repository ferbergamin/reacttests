import React from 'react';

export const userContext = React.createContext({
    idUser: "",
    nome: "",
    email: "",
    username: "",
    criadoEm: "",
    role: "",
    getUser: (token: string) => { }
});