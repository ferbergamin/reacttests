import React from 'react';
import { IonIcon, IonTabs, IonTabBar, IonTabButton, IonLabel, IonRouterOutlet } from '@ionic/react';
import { informationCircle, cart, home, person, menu } from 'ionicons/icons';
import { Home, Fim, Perfil, Menu, Shop } from '../pages';
import { Route, Redirect } from 'react-router-dom';
import { IonReactRouter } from '@ionic/react-router';

export const Header: React.FC = () => {
    // function logOutFunction() { localStorage.removeItem("token"); window.location.href = "/" };
    return (
        <>
            <IonReactRouter>
                <IonTabs>
                    <IonRouterOutlet>
                        <Route exact path="/home" component={Home} />
                        <Route exact path="/end" component={Fim} />
                        <Route exact path="/perfil" component={Perfil} />
                        <Route exact path="/menu" component={Menu} />
                        <Route exact path="/shop" component={Shop} />
                        <Route exact path="/" render={() => <Redirect to="/home" />} />
                    </IonRouterOutlet>
                    <IonTabBar slot="top">
                        <IonTabButton href="/home" tab="home">
                            <IonIcon icon={home} />
                            <IonLabel>Início</IonLabel>
                        </IonTabButton>

                        <IonTabButton href="/shop" tab="shop">
                            <IonIcon icon={cart} />
                            <IonLabel>Shop</IonLabel>
                        </IonTabButton>

                        <IonTabButton href="/end" tab="end">
                            <IonIcon icon={informationCircle} />
                            <IonLabel>Sobre</IonLabel>
                        </IonTabButton>

                        <IonTabButton href="/perfil" tab="perfil">
                            <IonIcon icon={person} />
                            <IonLabel>Perfil</IonLabel>
                        </IonTabButton>

                        <IonTabButton href="/menu" tab="menu">
                            <IonIcon icon={menu} />
                            <IonLabel>Menu</IonLabel>
                        </IonTabButton>
                    </IonTabBar>
                </IonTabs>
            </IonReactRouter>
        </>
    )
}