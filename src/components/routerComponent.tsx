import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import RouterController from '../helpers/routerController';
import { Login, Home, Fim } from '../pages';
import { userContext } from '../helpers/userContext';
import { IonRouterOutlet, IonContent } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

class RouterComponent extends Component {

    render() {
        return (
            <>
                <IonContent>
                    <IonReactRouter>
                        <IonRouterOutlet>
                            <Route path="/"
                                render={() => <Redirect to="/login" />}
                            />
                            <Route path="/login"
                                render={() => {
                                    if (this.context.idUser) {
                                        return <Redirect to="/home" />
                                    } else {
                                        return <Login />
                                    }
                                }}
                            />
                            <RouterController exact path="/home" component={Home} />
                            <Route exact path="/end" component={Fim} />
                        </IonRouterOutlet>
                    </IonReactRouter>
                </IonContent>
            </>

        )
    }
}
RouterComponent.contextType = userContext;
export default RouterComponent;